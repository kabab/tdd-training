import org.junit.Assert;
import org.junit.Test;

public class RomanNumbersTest {


    @Test
    public void shouldReturnI4One(){
        RomanNumbers romanNumbers = new RomanNumbers();
        Assert.assertEquals("I", romanNumbers.convert(1));
    }

    @Test
    public void shouldReturnII4Two(){
        RomanNumbers romanNumbers = new RomanNumbers();
        Assert.assertEquals("II", romanNumbers.convert(2));
    }

    @Test
    public void shouldReturnV4Five(){
        RomanNumbers romanNumbers = new RomanNumbers();
        Assert.assertEquals("V", romanNumbers.convert(5));
    }

    @Test
    public void shouldReturnX4Ten(){
        RomanNumbers romanNumbers = new RomanNumbers();
        Assert.assertEquals("X", romanNumbers.convert(10));
    }

    @Test
    public void shouldReturnXIForEleven(){
        RomanNumbers romanNumbers = new RomanNumbers();
        Assert.assertEquals("XI", romanNumbers.convert(11));
    }

    @Test
    public void shouldReturnXIIForTwelve(){
        RomanNumbers romanNumbers = new RomanNumbers();
        Assert.assertEquals("XII", romanNumbers.convert(12));
    }

    @Test
    public void shouldReturnXIVForForteen(){
        RomanNumbers romanNumbers = new RomanNumbers();
        Assert.assertEquals("XIV", romanNumbers.convert(14));
    }

    @Test
    public void shouldReturnXXXVForForThirtyFive(){
        RomanNumbers romanNumbers = new RomanNumbers();
        Assert.assertEquals("XXXV", romanNumbers.convert(35));
    }

    @Test
    public void shouldReturnXCVIIIFor98(){
        RomanNumbers romanNumbers = new RomanNumbers();
        Assert.assertEquals("XCVIII", romanNumbers.convert(98));
    }
}
