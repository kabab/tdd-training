import org.junit.Assert;
import org.junit.Test;

public class FizzBuzzTest {

    @Test
    public void shouldReturnTheSame(){
        Assert.assertEquals("4", FizzBuzz.run(4));
    }

    @Test
    public void shouldReturn2For2(){
        Assert.assertEquals("2", FizzBuzz.run(2));
    }

    @Test
    public void shouldReturnFizzFor6(){
        Assert.assertEquals("Fizz", FizzBuzz.run(6));
    }

    @Test
    public void shouldReturnFizzFor9(){
        Assert.assertEquals("Fizz", FizzBuzz.run(9));
    }

    @Test
    public void shouldReturnBuzzFor5(){
        Assert.assertEquals("Buzz", FizzBuzz.run(5));
    }

    @Test
    public void shouldReturnFizzBuzzFor15(){
        Assert.assertEquals("FizzBuzz", FizzBuzz.run(15));
    }

}
