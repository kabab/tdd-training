import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

public class StringToolsTest {

    private ReaderCustom scanner;


    @Test
    public void shouldReturnEmpty4EmtyString(){
        String ret = new StringTools().inverse("");
        Assert.assertEquals("", ret);
    }

    @Test
    public void shouldReturnInversedCharString(){

        String ret = new StringTools().inverse("a");
        Assert.assertEquals("a", ret);
    }

    @Test
    public void shouldReturnInversedString(){

        String ret = new StringTools().inverse("abc");
        Assert.assertEquals("cba", ret);
    }



    @Test
    public void shouldReadFromStdinWith2ndMock() {
        Scanner scanner = new Scanner(new ByteArrayInputStream("abc\n".getBytes()));


        StringTools stringTools = new StringTools(scanner);
        String result = stringTools.readFromStdin();
        Assert.assertEquals("cba", result);

    }
}
