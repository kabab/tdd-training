import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

public class MainTest {

    private Main main;


    @Before
    public void setup() {
        main = new Main();
    }

    @Test
    public void fibShouldReturn1for0() {
        BigInteger fib = main.fibMouhcine(0);
        Assert.assertEquals(fib, BigInteger.valueOf(1));
    }

    @Test
    public void fibShouldReturn1for1() {
        BigInteger fib = main.fibMouhcine(1);
        Assert.assertEquals(fib, BigInteger.valueOf(1));
    }

    @Test
    public void fibShouldReturn8for5() {
        BigInteger fib = main.fibMouhcine(5);
        Assert.assertEquals(fib, BigInteger.valueOf(8));
    }

    @Test
    public void fibShouldReturnNfor100000() {
        BigInteger fib = main.fibMouhcine(30);
        Assert.assertNotNull(fib);
    }

    @Test
    public void fibWithNegativeInputShouldReturn0() {
        BigInteger fib = main.fibMouhcine(-5);
        Assert.assertEquals(fib, BigInteger.valueOf(0));
    }
}
