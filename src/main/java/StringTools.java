import java.util.Scanner;

public class StringTools {
    Scanner scanner;

    public StringTools(){
        this.scanner = new Scanner(System.in);
    }

    public StringTools(Scanner scanner){
        this.scanner = scanner;
    }

    public String inverse(String string) {
        StringBuilder str = new StringBuilder(string);
        return str.reverse().toString();
    }

    public String readFromStdin() {
            String line = scanner.nextLine();
            return inverse(line);
    }
}
