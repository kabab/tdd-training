import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RomanNumbers {
    public String convert(int number) {

        List<String> romainSymbols = Arrays.asList("", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X");
        List<String> romainSymbolsX = Arrays.asList("", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC", "C");

        StringBuffer result = new StringBuffer();

        int xIndex = number / 10;
        int rIndex = number % 10;

        result.append(romainSymbolsX.get(xIndex)).append(romainSymbols.get(rIndex));

        return result.toString();
    }
}
