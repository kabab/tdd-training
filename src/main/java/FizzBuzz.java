public class FizzBuzz {
    public static String run(int i)
        {
        StringBuffer res= new StringBuffer();

        if( i % 3 == 0 )
            res.append("Fizz");
        if( i % 5 == 0 )
            res.append("Buzz");

        return res.length() == 0 ? i+"" : res.toString();
    }
}
